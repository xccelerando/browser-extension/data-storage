Implementation of the data store module for the browser plugin. General / initial requirements:

1. Persistent;
2. Flexible structure (i.e. no schema);
3. Client side encryption for max security and privacy;
4. Lightweight;
5. Multi-user;
6. Ability to host database by different data controllers / providers and possibly client-side.